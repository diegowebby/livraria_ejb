package br.com.caelum.livraria.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.caelum.livraria.modelo.Autor;
import br.com.caelum.livraria.modelo.Livro;
import br.com.caelum.livraria.modelo.Usuario;

@Stateless
public class UsuarioDao implements Serializable {

	private static final long serialVersionUID = -5567677793419624127L;

	@PersistenceContext
	private EntityManager manager;
	@Inject
	private LivroDao livroDao;

	public boolean existe(Usuario usuario) {

		TypedQuery<Usuario> query = manager.createQuery("select u from Usuario u where u.email = :pEmail and u.senha = :pSenha", Usuario.class);

		query.setParameter("pEmail", usuario.getEmail());
		query.setParameter("pSenha", usuario.getSenha());

		List<Livro> livros = livroDao.listaTodos();

		for (Livro livro : livros) {
			System.out.println(livro.getTitulo());
			for (Autor autor : livro.getAutores()) {
				System.out.println(autor.getNome());
			}
		}

		try {
			query.getSingleResult();
		} catch (NoResultException ex) {
			return false;
		}

		return true;
	}

}
