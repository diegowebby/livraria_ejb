package livraria;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.caelum.livraria.bean.LoginBean;
import br.com.caelum.livraria.dao.UsuarioDao;
import br.com.caelum.livraria.modelo.Usuario;

@RunWith(Arquillian.class)
public class LoginTest {

	/*
	 * @Inject
	 * UsuarioDao usuarioDao = new UsuarioDao();
	 * 
	 * @PersistenceContext(unitName = "livraria")
	 * EntityManager em;
	 */

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class).addClass(LoginBean.class).addAsResource("persistence.xml", "META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	LoginBean loginBean;

	@Inject
	UsuarioDao usuarioDao = new UsuarioDao();

	@PersistenceContext(unitName = "livraria")
	EntityManager em;

	@Inject
	private EntityManager ema;// como é uma interface, precisa de um produtor.

	/*
	 * @Deployment
	 * public static Archive<?> createDeployment() {
	 * return ShrinkWrap.create(WebArchive.class, "test.war").addPackage(UsuarioDao.class.getPackage()).addAsResource("persistence.xml", "META-INF/persistence.xml")
	 * .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	 * }
	 */

	@Test
	public void testaLogin() {

		Usuario usuario = new Usuario();
		usuario.setEmail("diegowebby@gmail.com");
		usuario.setSenha("123");
		usuarioDao.existe(usuario);

		assertEquals("oi", "oi");

	}

}
